# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180220181641) do

  create_table "aliances", force: :cascade do |t|
    t.string   "image",         limit: 255
    t.string   "name",          limit: 255
    t.string   "external_link", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "banners", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "image",         limit: 255
    t.string   "external_link", limit: 255
    t.boolean  "public",        limit: 1
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string   "documentation", limit: 255
    t.string   "name",          limit: 255
    t.string   "company",       limit: 255
    t.string   "email",         limit: 255
    t.string   "telephone",     limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "particular",    limit: 255
    t.string   "agency",        limit: 255
    t.string   "city",          limit: 255
    t.string   "country",       limit: 255
  end

  create_table "events", force: :cascade do |t|
    t.integer  "client_id",           limit: 4
    t.string   "date_event",          limit: 255
    t.string   "event_type",          limit: 255
    t.string   "quantity",            limit: 255
    t.string   "begin_date",          limit: 255
    t.string   "end_date",            limit: 255
    t.string   "begin_time",          limit: 255
    t.string   "end_time",            limit: 255
    t.string   "assembly_type",       limit: 255
    t.text     "event_description",   limit: 65535
    t.boolean  "cold_snacks",         limit: 1
    t.boolean  "hot_snacks",          limit: 1
    t.boolean  "dish_menu",           limit: 1
    t.boolean  "buffet_type",         limit: 1
    t.boolean  "theme_stations",      limit: 1
    t.boolean  "morning_coffeebreak", limit: 1
    t.boolean  "evening_coffeebreak", limit: 1
    t.boolean  "cheese_table",        limit: 1
    t.boolean  "uncorking_coctels",   limit: 1
    t.boolean  "uncorking_whisky",    limit: 1
    t.boolean  "uncorking_wine",      limit: 1
    t.boolean  "uncorking_vodka",     limit: 1
    t.boolean  "coctel_bar",          limit: 1
    t.boolean  "sodas",               limit: 1
    t.string   "color_theme",         limit: 255
    t.boolean  "flower_arrengements", limit: 1
    t.boolean  "baloons",             limit: 1
    t.boolean  "toldo",               limit: 1
    t.boolean  "sound",               limit: 1
    t.boolean  "dj",                  limit: 1
    t.boolean  "musical_group",       limit: 1
    t.boolean  "podium",              limit: 1
    t.boolean  "videobeam",           limit: 1
    t.boolean  "lights",              limit: 1
    t.boolean  "inflatable",          limit: 1
    t.boolean  "magic_show",          limit: 1
    t.boolean  "dance_show",          limit: 1
    t.boolean  "crazy_hour",          limit: 1
    t.text     "other_services",      limit: 65535
    t.text     "observations",        limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "coffee_station",      limit: 1
    t.boolean  "hidra_station",       limit: 1
    t.boolean  "buffet_breakfast",    limit: 1
    t.boolean  "buffet_lunch",        limit: 1
    t.boolean  "buffet_dinner",       limit: 1
    t.boolean  "close_lunch",         limit: 1
    t.boolean  "close_dinner",        limit: 1
    t.text     "theme_description",   limit: 65535
    t.boolean  "circle_table",        limit: 1
    t.boolean  "square_table",        limit: 1
    t.boolean  "big_table",           limit: 1
    t.boolean  "screen_display",      limit: 1
    t.boolean  "wireless_micro",      limit: 1
    t.boolean  "wire_micro",          limit: 1
    t.boolean  "porfolio",            limit: 1
    t.boolean  "laptop",              limit: 1
    t.boolean  "tarima",              limit: 1
  end

  create_table "events_rooms", force: :cascade do |t|
    t.integer  "event_id",   limit: 4
    t.integer  "room_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "quantity",   limit: 4
  end

  add_index "events_rooms", ["event_id"], name: "index_events_rooms_on_event_id", using: :btree
  add_index "events_rooms", ["room_id"], name: "index_events_rooms_on_room_id", using: :btree

  create_table "images", force: :cascade do |t|
    t.string   "img",        limit: 255
    t.integer  "room_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "room_id",        limit: 4
    t.integer  "quantity",       limit: 4
    t.integer  "reservation_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "reservations", force: :cascade do |t|
    t.integer  "client_id",    limit: 4
    t.integer  "adults",       limit: 4
    t.integer  "kids",         limit: 4
    t.integer  "babies",       limit: 4
    t.string   "origin",       limit: 255
    t.string   "motive",       limit: 255
    t.date     "checkin"
    t.date     "checkout"
    t.integer  "payment",      limit: 4
    t.text     "observations", limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "breakfast",    limit: 1
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.float    "price",       limit: 24
    t.text     "description", limit: 65535
    t.string   "cover",       limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "permalink",   limit: 255
  end

  create_table "surveys", force: :cascade do |t|
    t.string   "ip",                         limit: 255
    t.string   "name",                       limit: 255
    t.string   "email",                      limit: 255
    t.string   "room",                       limit: 255
    t.date     "checkin"
    t.date     "checkout"
    t.text     "comment",                    limit: 65535
    t.integer  "welcome_attention",          limit: 4
    t.integer  "welcome_professionality",    limit: 4
    t.integer  "welcome_decoration",         limit: 4
    t.integer  "welcome_butler",             limit: 4
    t.integer  "welcome_info_quality",       limit: 4
    t.integer  "room_cleaning",              limit: 4
    t.integer  "room_comfort",               limit: 4
    t.integer  "room_decoration",            limit: 4
    t.integer  "room_cleanliness",           limit: 4
    t.integer  "room_channels",              limit: 4
    t.integer  "room_soundless",             limit: 4
    t.integer  "facilities_lobby",           limit: 4
    t.integer  "facilities_business_center", limit: 4
    t.integer  "facilities_gym",             limit: 4
    t.integer  "facilities_stores",          limit: 4
    t.string   "facilities_other",           limit: 255
    t.integer  "facilities_other_value",     limit: 4
    t.integer  "lobby_breakfast",            limit: 4
    t.integer  "lobby_menu",                 limit: 4
    t.integer  "lobby_waiting",              limit: 4
    t.integer  "lobby_service",              limit: 4
    t.integer  "lobby_staff",                limit: 4
    t.integer  "lobby_quality_price",        limit: 4
    t.integer  "terrace_breakfast",          limit: 4
    t.integer  "terrace_menu",               limit: 4
    t.integer  "terrace_waiting",            limit: 4
    t.integer  "terrace_service",            limit: 4
    t.integer  "terrace_staff",              limit: 4
    t.integer  "terrace_quality_price",      limit: 4
    t.integer  "staff_professionalism",      limit: 4
    t.integer  "staff_simpathy",             limit: 4
    t.integer  "staff_speed",                limit: 4
    t.integer  "staff_effectiveness",        limit: 4
    t.integer  "staff_availability",         limit: 4
    t.integer  "staff_cleanliness",          limit: 4
    t.integer  "met_by",                     limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "total_excellent",            limit: 4
    t.integer  "total_good",                 limit: 4
    t.integer  "total_regular",              limit: 4
    t.integer  "total_bad",                  limit: 4
  end

  create_table "terms", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "permalink",              limit: 255
    t.string   "username",               limit: 255
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "events_rooms", "events"
  add_foreign_key "events_rooms", "rooms"
end
