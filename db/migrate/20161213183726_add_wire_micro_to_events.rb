class AddWireMicroToEvents < ActiveRecord::Migration
  def change
    add_column :events, :wire_micro, :boolean
  end
end
