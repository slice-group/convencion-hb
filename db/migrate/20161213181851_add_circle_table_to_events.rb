class AddCircleTableToEvents < ActiveRecord::Migration
  def change
    add_column :events, :circle_table, :boolean
  end
end
