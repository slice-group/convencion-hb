class AddSquareTableToEvents < ActiveRecord::Migration
  def change
    add_column :events, :square_table, :boolean
  end
end
