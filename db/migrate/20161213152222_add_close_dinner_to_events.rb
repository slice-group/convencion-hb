class AddCloseDinnerToEvents < ActiveRecord::Migration
  def change
    add_column :events, :close_dinner, :boolean
  end
end
