class AddCloseLunchToEvents < ActiveRecord::Migration
  def change
    add_column :events, :close_lunch, :boolean
  end
end
