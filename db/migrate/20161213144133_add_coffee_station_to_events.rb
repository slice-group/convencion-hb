class AddCoffeeStationToEvents < ActiveRecord::Migration
  def change
    add_column :events, :coffee_station, :boolean
  end
end
