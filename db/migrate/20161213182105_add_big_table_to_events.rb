class AddBigTableToEvents < ActiveRecord::Migration
  def change
    add_column :events, :big_table, :boolean
  end
end
