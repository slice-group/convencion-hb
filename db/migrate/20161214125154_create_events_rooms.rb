class CreateEventsRooms < ActiveRecord::Migration
  def change
    create_table :events_rooms do |t|
      t.belongs_to :event, index: true
      t.belongs_to :room, index: true

      t.timestamps null: false
    end
    add_foreign_key :events_rooms, :events
    add_foreign_key :events_rooms, :rooms
  end
end
