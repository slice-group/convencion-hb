class AddBreakfastToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :breakfast, :boolean
  end
end
