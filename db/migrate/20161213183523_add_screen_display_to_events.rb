class AddScreenDisplayToEvents < ActiveRecord::Migration
  def change
    add_column :events, :screen_display, :boolean
  end
end
