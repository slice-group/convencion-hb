class AddBuffetBreakfastToEvents < ActiveRecord::Migration
  def change
    add_column :events, :buffet_breakfast, :boolean
  end
end
