class AddWirelessMicroToEvents < ActiveRecord::Migration
  def change
    add_column :events, :wireless_micro, :boolean
  end
end
