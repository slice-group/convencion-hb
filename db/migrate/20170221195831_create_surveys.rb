class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.string :ip
      t.string :name
      t.string :email
      t.string :room
      t.string :checkin
      t.string :checkout
      t.text :comment
      t.integer :welcome_attention
      t.integer :welcome_professionality
      t.integer :welcome_decoration
      t.integer :welcome_butler
      t.integer :welcome_info_quality
      t.integer :room_cleaning
      t.integer :room_comfort
      t.integer :room_decoration
      t.integer :room_cleanliness
      t.integer :room_channels
      t.integer :room_soundless
      t.integer :facilities_lobby
      t.integer :facilities_business_center
      t.integer :facilities_gym
      t.integer :facilities_stores
      t.string :facilities_other
      t.integer :facilities_other_value
      t.integer :lobby_breakfast
      t.integer :lobby_manu
      t.integer :lobby_waiting
      t.integer :lobby_service
      t.integer :lobby_staff
      t.integer :lobby_quality_price
      t.integer :terrace_breakfast
      t.integer :terrace_manu
      t.integer :terrace_waiting
      t.integer :terrace_service
      t.integer :terrace_staff
      t.integer :terrace_quality_price
      t.integer :staff_professionalism
      t.integer :staff_simpathy
      t.integer :staff_speed
      t.integer :staff_effectiveness
      t.integer :staff_availability
      t.integer :staff_cleanliness
      t.integer :met_by

      t.timestamps null: false
    end
  end
end
