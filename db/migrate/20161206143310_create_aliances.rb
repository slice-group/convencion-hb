class CreateAliances < ActiveRecord::Migration
  def change
    create_table :aliances do |t|
      t.string :image
      t.string :name
      t.string :external_link

      t.timestamps null: false
    end
  end
end
