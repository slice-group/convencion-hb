class AddBuffetLunchToEvents < ActiveRecord::Migration
  def change
    add_column :events, :buffet_lunch, :boolean
  end
end
