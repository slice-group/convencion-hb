class AddFieldsToSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :total_excellent, :integer
    add_column :surveys, :total_good, :integer
    add_column :surveys, :total_regular, :integer
    add_column :surveys, :total_bad, :integer
  end
end
