class AddThemeDescriptionToEvents < ActiveRecord::Migration
  def change
    add_column :events, :theme_description, :text
  end
end
