class AddLaptopToEvents < ActiveRecord::Migration
  def change
    add_column :events, :laptop, :boolean
  end
end
