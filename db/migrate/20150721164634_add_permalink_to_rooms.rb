class AddPermalinkToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :permalink, :string
  end
end
