class AddBuffetDinnerToEvents < ActiveRecord::Migration
  def change
    add_column :events, :buffet_dinner, :boolean
  end
end
