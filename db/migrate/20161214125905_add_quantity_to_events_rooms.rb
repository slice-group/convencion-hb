class AddQuantityToEventsRooms < ActiveRecord::Migration
  def change
    add_column :events_rooms, :quantity, :integer
  end
end
