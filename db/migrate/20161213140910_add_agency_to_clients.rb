class AddAgencyToClients < ActiveRecord::Migration
  def change
    add_column :clients, :agency, :string
  end
end
