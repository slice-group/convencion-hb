class AddHidraStationToEvents < ActiveRecord::Migration
  def change
    add_column :events, :hidra_station, :boolean
  end
end
