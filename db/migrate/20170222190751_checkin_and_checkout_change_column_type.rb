class CheckinAndCheckoutChangeColumnType < ActiveRecord::Migration
  def change
    change_column :surveys, :checkin, :date
    change_column :surveys, :checkout, :date
    rename_column :surveys, :lobby_manu, :lobby_menu
    rename_column :surveys, :terrace_manu, :terrace_menu
  end
end
