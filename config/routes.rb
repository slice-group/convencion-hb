Rails.application.routes.draw do

  root to: 'frontend#index'

  devise_for :users, skip: KepplerConfiguration.skip_module_devise

  resources :admin, only: :index

  get "/eventos" => 'events#new', :as => :new_event
  post "/eventos" => 'events#create', :as => :create_events
  get "/habitaciones/:permalink" => 'frontend#show_room' , :as => :show_room
  get "/restaurantes" => "frontend#restaurants" , as: :restaurants
  get "/merida" => "frontend#merida" , as: :merida
  get "/reservaciones" => 'reservations#new' , as: :new_reservation
  post "/reservaciones" => 'reservations#create' , as: :create_reservation
  get "/encuesta" => 'surveys#new' , as: :new_survey
  post "/encuesta" => 'surveys#create' , as: :create_survey
  post "/encuesta/validate" => 'surveys#validate' , as: :validate_survey

  scope :admin do

  	resources :users do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :terms do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :aliances do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :banners do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :rooms do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :clients do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :reservations, except: [:new, :create] do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :events, except: [:new, :create] do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :surveys, except: [:new, :create] do
      get '(page/:page)', action: :index, on: :collection, as: ''
      get '/resultados', action: :result_survey, as: :result, on: :collection
      get '/busqueda', action: :search_surveys_range_days, as: :search_surveys_range_days, on: :collection
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

  end


  #errors
  match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all


  #dashboard
  mount KepplerGaDashboard::Engine, :at => '/', as: 'dashboard'


end
