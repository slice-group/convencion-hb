module SurveysHelper
  def build_tables
    [
      [ 'Bienvenida', welcoming_attr ],
      [ 'Habitaciones', room_attr ],
      [ 'Instalaciones', facilities_attr ],
      [ 'Restaurant Lobby “El Encuentro Restaurant”', lobby_attr ],
      [ 'Restaurant Terraza “Les Golfes Gastrobar”', terrace_attr ],
      [ 'Personal del Hotel', staff_attr ]
    ]
  end

  def welcoming_attr
    [
      'welcome_attention', 'welcome_professionality', 'welcome_decoration',
      'welcome_butler', 'welcome_info_quality'
    ]
  end

  def room_attr
    %w(
      room_cleaning room_comfort room_decoration
      room_cleanliness room_channels room_soundless
    )
  end

  def facilities_attr
    %w(
      facilities_lobby facilities_business_center
      facilities_gym facilities_stores facilities_other
    )
  end

  def lobby_attr
    %w(
      lobby_breakfast lobby_menu lobby_waiting
      lobby_service lobby_staff lobby_quality_price
    )
  end

  def terrace_attr
    %w(
      terrace_breakfast terrace_menu terrace_waiting
      terrace_service terrace_staff terrace_quality_price
    )
  end

  def staff_attr
    %w(
      staff_professionalism staff_simpathy staff_speed
      staff_effectiveness staff_availability staff_cleanliness
    )
  end

  def important_attributes
    %w(
      welcome_attention welcome_professionality welcome_decoration
      welcome_butler welcome_info_quality room_cleaning room_comfort
      room_decoration room_cleanliness room_channels room_soundless
      facilities_lobby staff_professionalism staff_simpathy staff_speed
      staff_effectiveness staff_availability staff_cleanliness met_by
    )
  end
end
