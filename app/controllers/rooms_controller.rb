#Generado con Keppler.
class RoomsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_room, only: [:show, :edit, :update, :edit_room_images, :update_room_images, :destroy]

  # GET /rooms
  def index
    rooms = Room.searching(@query).all
    @objects, @total = rooms.page(@current_page), rooms.size
    redirect_to rooms_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /rooms/1
  def show
  end

  # GET /rooms/new
  def new
    @room = Room.new
  end

  # GET /rooms/1/edit
  def edit
  end

  # POST /rooms
  def create
    @room = Room.new(room_params)

    if @room.save
      redirect_to @room, notice: 'Habitación creada satisfactoriamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /rooms/1
  def update
    if @room.update(room_params)
      redirect_to @room, notice: 'Habitación editada satisfactoriamente.'
    else
      render :edit
    end
  end


  
  # DELETE /rooms/1
  def destroy
    @room.destroy
    redirect_to rooms_url, notice: 'Habitación eliminada satisfactoriamente.'
  end

  def destroy_multiple
    Room.destroy redefine_ids(params[:multiple_ids])
    redirect_to rooms_path(page: @current_page, search: @query), notice: "Habitaciones eliminadas satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def room_params
      params.require(:room).permit(:name, :price, :description, :cover, images_attributes: [:id, :img, :_destroy])
    end
end
