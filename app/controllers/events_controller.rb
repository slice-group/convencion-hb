#Generado con Keppler.
class EventsController < ApplicationController
  before_filter :authenticate_user!, except: [:new, :create]
  layout :resolve_layout
  load_and_authorize_resource except: [:new, :create]
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  # GET /events
  def index
    events = Event.searching(@query).all
    @objects, @total = events.page(@current_page), events.size
    redirect_to events_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /events/1
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
    @event.client = Client.new
    @terms = Term.find_by_name("Eventos")
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  def create
    params[:event].parse_time_select!(:begin_time)
    params[:event].parse_time_select!(:end_time)
    if !event_params[:begin_time].to_s.empty? && !event_params[:end_time].to_s.empty?
      params[:event][:begin_time] = params[:event][:begin_time].strftime("%I:%M %p")
      params[:event][:end_time] = params[:event][:end_time].strftime("%I:%M %p")
    end
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        EventMailer.event_admin(@event).deliver_now
        EventMailer.event_user(@event).deliver_now
        format.html { redirect_to root_path, notice: @event.client.name+', La solicitud de reserva para su evento ya ha sido enviada.' }
        format.json { render action: 'show', status: :created, location: @event }
      else
        format.html { render action: 'new' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  def update
    if @event.update(event_params)
      redirect_to @event, notice: 'Event was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /events/1
  def destroy
    @event.destroy
    redirect_to events_url, notice: 'Event was successfully destroyed.'
  end

  def destroy_multiple
    Event.destroy redefine_ids(params[:multiple_ids])
    redirect_to events_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def event_params
      params.require(:event).permit(:date_event, :event_type, :quantity,
       :begin_date, :end_date, :begin_time, :end_time, :assembly_type,
       :event_description, :cold_snacks, :hot_snacks, :buffet_breakfast,
       :buffet_lunch, :buffet_dinner, :close_lunch,
        :close_dinner, :theme_description, :circle_table, :square_table,
        :big_table,:dish_menu, :coffee_station, :hidra_station, :theme_stations,
        :morning_coffeebreak, :evening_coffeebreak, :cheese_table, :uncorking_coctels,
        :uncorking_whisky, :uncorking_wine, :uncorking_vodka, :coctel_bar, :sodas,
         :color_theme, :flower_arrengements, :baloons, :toldo, :sound, :dj,
         :musical_group, :porfolio, :laptop, :tarima, :podium, :videobeam, :screen_display,
         :wireless_micro, :wire_micro, :lights, :inflatable,
         :magic_show, :dance_show, :crazy_hour, :other_services, :observations,
         client_attributes: [:name, :documentation, :company, :telephone,
           :email, :particular, :agency, :city, :country], events_rooms_attributes: [:id, :room_id, :quantity])
    end

    def resolve_layout
      case action_name
      when "destroy", "update", "edit", "show", "index"
        'admin/application'
      else
        'frontend/application'
      end
    end
end
