#Generado con Keppler.
class BannersController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_banner, only: [:show, :edit, :update, :destroy]

  # GET /banners
  def index
    banners = Banner.searching(@query).all
    @objects, @total = banners.page(@current_page), banners.size
    redirect_to banners_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /banners/1
  def show
  end

  # GET /banners/new
  def new
    @banner = Banner.new
  end

  # GET /banners/1/edit
  def edit
  end

  # POST /banners
  def create
    @banner = Banner.new(banner_params)

    if @banner.save
      redirect_to @banner, notice: 'Banner was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /banners/1
  def update
    if @banner.update(banner_params)
      redirect_to @banner, notice: 'Banner was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /banners/1
  def destroy
    @banner.destroy
    redirect_to banners_url, notice: 'Banner was successfully destroyed.'
  end

  def destroy_multiple
    Banner.destroy redefine_ids(params[:multiple_ids])
    redirect_to banners_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_banner
      @banner = Banner.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def banner_params
      params.require(:banner).permit(:name, :image, :external_link, :public)
    end
end
