#Generado con Keppler.
class AliancesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_aliance, only: [:show, :edit, :update, :destroy]

  # GET /aliances
  def index
    aliances = Aliance.searching(@query).all
    @objects, @total = aliances.page(@current_page), aliances.size
    redirect_to aliances_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /aliances/1
  def show
  end

  # GET /aliances/new
  def new
    @aliance = Aliance.new
  end

  # GET /aliances/1/edit
  def edit
  end

  # POST /aliances
  def create
    @aliance = Aliance.new(aliance_params)

    if @aliance.save
      redirect_to @aliance, notice: 'Aliance was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /aliances/1
  def update
    if @aliance.update(aliance_params)
      redirect_to @aliance, notice: 'Aliance was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /aliances/1
  def destroy
    @aliance.destroy
    redirect_to aliances_url, notice: 'Aliance was successfully destroyed.'
  end

  def destroy_multiple
    Aliance.destroy redefine_ids(params[:multiple_ids])
    redirect_to aliances_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_aliance
      @aliance = Aliance.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def aliance_params
      params.require(:aliance).permit(:image, :name, :external_link)
    end
end
