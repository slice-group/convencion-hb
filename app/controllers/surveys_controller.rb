#Generado con Keppler.
class SurveysController < ApplicationController
  include SurveysHelper
  before_filter :authenticate_user!, except: [:new, :create, :validate]
  layout :resolve_layout
  load_and_authorize_resource except: [:new, :create]
  before_action :set_survey, only: [:show, :edit, :update, :destroy]

  # GET /surveys
  def index
    surveys = Survey.searching(@query).all
    @objects, @total = surveys.page(@current_page), surveys.size
    redirect_to surveys_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /surveys/1
  def show
    respond_to do |format|
      format.html { render action: 'show' }
      format.pdf { render pdf: 'Encuesta', template: 'surveys/show.pdf.haml', handlers: [:haml] }
      format.xlsx do
        @build_tables = build_tables
        render xlsx: 'show', filename: "Encuesta_#{@survey.id} #{@survey.created_at.strftime('%Y-%m-%d %I:%M')}.xlsx"
      end
    end
  end

  def search_surveys_range_days
    @survey = Survey.new(survey_params)
    @result1 = Survey.where(["checkin >= '#{Date.parse("#{@survey.email}")}' AND checkout <= '#{Date.parse("#{@survey.room}")}'"])
    @welcome_attention_4 = 0
    @welcome_attention_3 = 0
    @welcome_attention_2 = 0
    @welcome_attention_1 = 0

    @welcome_professionality_4 = 0
    @welcome_professionality_3 = 0
    @welcome_professionality_2 = 0
    @welcome_professionality_1 = 0

    @welcome_decoration_4 = 0
    @welcome_decoration_3 = 0
    @welcome_decoration_2 = 0
    @welcome_decoration_1 = 0

    @welcome_butler_4 = 0
    @welcome_butler_3 = 0
    @welcome_butler_2 = 0
    @welcome_butler_1 = 0

    @welcome_info_quality_4 = 0
    @welcome_info_quality_3 = 0
    @welcome_info_quality_2 = 0
    @welcome_info_quality_1 = 0

    @welcome_info_quality_4 = 0
    @welcome_info_quality_3 = 0
    @welcome_info_quality_2 = 0
    @welcome_info_quality_1 = 0

    #--- 2 parte encuesta-----

    @room_cleaning_4 = 0
    @room_cleaning_3 = 0
    @room_cleaning_2 = 0
    @room_cleaning_1 = 0

    @room_comfort_4 = 0
    @room_comfort_3 = 0
    @room_comfort_2 = 0
    @room_comfort_1 = 0

    @room_decoration_4 = 0
    @room_decoration_3 = 0
    @room_decoration_2 = 0
    @room_decoration_1 = 0

    @room_cleanliness_4 = 0
    @room_cleanliness_3 = 0
    @room_cleanliness_2 = 0
    @room_cleanliness_1 = 0

    @room_channels_4 = 0
    @room_channels_3 = 0
    @room_channels_2 = 0
    @room_channels_1 = 0

    @room_soundless_4 = 0
    @room_soundless_3 = 0
    @room_soundless_2 = 0
    @room_soundless_1 = 0

    #-- 3 parte ---- 

    @facilities_lobby_4 = 0
    @facilities_lobby_3 = 0
    @facilities_lobby_2 = 0
    @facilities_lobby_1 = 0

    #-- 4 parte ----

    @staff_professionalism_4 = 0
    @staff_professionalism_3 = 0
    @staff_professionalism_2 = 0
    @staff_professionalism_1 = 0

    @staff_simpathy_4 = 0
    @staff_simpathy_3 = 0
    @staff_simpathy_2 = 0
    @staff_simpathy_1 = 0

    @staff_speed_4 = 0
    @staff_speed_3 = 0
    @staff_speed_2 = 0
    @staff_speed_1 = 0

    @staff_effectiveness_4 = 0
    @staff_effectiveness_3 = 0
    @staff_effectiveness_2 = 0
    @staff_effectiveness_1 = 0

    @staff_availability_4 = 0
    @staff_availability_3 = 0
    @staff_availability_2 = 0
    @staff_availability_1 = 0

    @staff_cleanliness_4 = 0
    @staff_cleanliness_3 = 0
    @staff_cleanliness_2 = 0
    @staff_cleanliness_1 = 0

     #---- 5 parte ----- 
    @met_by_6 = 0
    @met_by_5 = 0
    @met_by_4 = 0
    @met_by_3 = 0
    @met_by_2 = 0
    @met_by_1 = 0


    @result1.each do |result|
      if result.welcome_attention == 4
        @welcome_attention_4 = @welcome_attention_4 + 1
      end
      if result.welcome_attention == 3
        @welcome_attention_3 = @welcome_attention_3 + 1
      end
      if result.welcome_attention == 2
        @welcome_attention_2 = @welcome_attention_2 + 1
      end
      if result.welcome_attention == 1
        @welcome_attention_1 = @welcome_attention_1 + 1
      end
      if result.welcome_professionality == 4
        @welcome_professionality_4 = @welcome_professionality_4 + 1
      end
      if result.welcome_professionality == 3
        @welcome_professionality_3 = @welcome_professionality_3 + 1
      end
      if result.welcome_professionality == 2
        @welcome_professionality_2 = @welcome_professionality_2 + 1
      end
      if result.welcome_professionality == 1
        @welcome_professionality_1 = @welcome_professionality_1 + 1
      end
      if result.welcome_decoration == 4
        @welcome_decoration_4 = @welcome_decoration_4 + 1
      end
      if result.welcome_decoration == 3
        @welcome_decoration_3 = @welcome_decoration_3 + 1
      end
      if result.welcome_decoration == 2
        @welcome_decoration_2 = @welcome_decoration_2 + 1
      end
      if result.welcome_decoration == 1
        @welcome_decoration_1 = @welcome_decoration_1 + 1
      end
      if result.welcome_butler == 4
        @welcome_butler_4 = @welcome_butler_4 + 1
      end
      if result.welcome_butler == 3
        @welcome_butler_3 = @welcome_butler_3 + 1
      end
      if result.welcome_butler == 2
        @welcome_butler_2 = @welcome_butler_2 + 1
      end
      if result.welcome_butler == 1
        @welcome_butler_1 = @welcome_butler_1 + 1
      end
      if result.welcome_info_quality == 4
        @welcome_info_quality_4 = @welcome_info_quality_4 + 1
      end
      if result.welcome_info_quality == 3
        @welcome_info_quality_3 = @welcome_info_quality_3 + 1
      end
      if result.welcome_info_quality == 2
        @welcome_info_quality_2 = @welcome_info_quality_2 + 1
      end
      if result.welcome_info_quality == 1
        @welcome_info_quality_1 = @welcome_info_quality_1 + 1
      end

      #-- 2 parte ------
      
      if result.room_cleaning == 4
        @room_cleaning_4 = @room_cleaning_4 + 1
      end
      if result.room_cleaning == 3
        @room_cleaning_3 = @room_cleaning_3 + 1
      end
      if result.room_cleaning == 2
        @room_cleaning_2 = @room_cleaning_2 + 1
      end
      if result.room_cleaning == 1
        @room_cleaning_1 = @room_cleaning_1 + 1
      end 

       if result.room_comfort == 4
        @room_comfort_4 = @room_comfort_4 + 1
      end
      if result.room_comfort == 3
        @room_comfort_3 = @room_comfort_3 + 1
      end
      if result.room_comfort == 2
        @room_comfort_2 = @room_comfort_2 + 1
      end
      if result.room_comfort == 1
        @room_comfort_1 = @room_comfort_1 + 1
      end

      if result.room_decoration == 4
        @room_decoration_4 = @room_decoration_4 + 1
      end
      if result.room_decoration == 3
        @room_decoration_3 = @room_decoration_3 + 1
      end
      if result.room_decoration == 2
        @room_decoration_2 = @room_decoration_2 + 1
      end
      if result.room_decoration == 1
        @room_decoration_1 = @room_decoration_1 + 1
      end 

      if result.room_cleanliness == 4
        @room_cleanliness_4 = @room_cleanliness_4 + 1
      end
      if result.room_cleanliness == 3
        @room_cleanliness_3 = @room_cleanliness_3 + 1
      end
      if result.room_cleanliness == 2
        @room_cleanliness_2 = @room_cleanliness_2 + 1
      end
      if result.room_cleanliness == 1
        @room_cleanliness_1 = @room_cleanliness_1 + 1
      end 

      if result.room_channels == 4
        @room_channels_4 = @room_channels_4 + 1
      end
      if result.room_channels == 3
        @room_channels_3 = @room_channels_3 + 1
      end
      if result.room_channels == 2
        @room_channels_2 = @room_channels_2 + 1
      end
      if result.room_channels == 1
        @room_channels_1 = @room_channels_1 + 1
      end

      if result.room_soundless == 4
        @room_soundless_4 = @room_soundless_4 + 1
      end
      if result.room_soundless == 3
        @room_soundless_3 = @room_soundless_3 + 1
      end
      if result.room_soundless == 2
        @room_soundless_2 = @room_soundless_2 + 1
      end
      if result.room_soundless == 1
        @room_soundless_1 = @room_soundless_1 + 1
      end

      #---- 3 parte-----

      if result.facilities_lobby == 4
        @facilities_lobby_4 = @facilities_lobby_4 + 1
      end
      if result.facilities_lobby == 3
        @facilities_lobby_3 = @facilities_lobby_3 + 1
      end
      if result.facilities_lobby == 2
        @facilities_lobby_2 = @facilities_lobby_2 + 1
      end
      if result.facilities_lobby == 1
        @facilities_lobby_1 = @facilities_lobby_1 + 1
      end

      #----- 4 parte ------  

      if result.staff_professionalism == 4
        @staff_professionalism_4 = @staff_professionalism_4 + 1
      end
      if result.staff_professionalism == 3
        @staff_professionalism_3 = @staff_professionalism_3 + 1
      end
      if result.staff_professionalism == 2
        @staff_professionalism_2 = @staff_professionalism_2 + 1
      end
      if result.staff_professionalism == 1
        @staff_professionalism_1 = @staff_professionalism_1 + 1
      end

      if result.staff_effectiveness == 4
        @staff_effectiveness_4 = @staff_effectiveness_4 + 1
      end
      if result.staff_effectiveness == 3
        @staff_effectiveness_3 = @staff_effectiveness_3 + 1
      end
      if result.staff_effectiveness == 2
        @staff_effectiveness_2 = @staff_effectiveness_2 + 1
      end
      if result.staff_effectiveness == 1
        @staff_effectiveness_1 = @staff_effectiveness_1 + 1
      end

      if result.staff_simpathy == 4
        @staff_simpathy_4 = @staff_simpathy_4 + 1
      end
      if result.staff_simpathy == 3
        @staff_simpathy_3 = @staff_simpathy_3 + 1
      end
      if result.staff_simpathy == 2
        @staff_simpathy_2 = @staff_simpathy_2 + 1
      end
      if result.staff_simpathy == 1
        @staff_simpathy_1 = @staff_simpathy_1 + 1
      end

      if result.staff_speed == 4
        @staff_speed_4 = @staff_speed_4 + 1
      end
      if result.staff_speed == 3
        @staff_speed_3 = @staff_speed_3 + 1
      end
      if result.staff_speed == 2
        @staff_speed_2 = @staff_speed_2 + 1
      end
      if result.staff_speed == 1
        @staff_speed_1 = @staff_speed_1 + 1
      end

      if result.staff_availability == 4
        @staff_availability_4 = @staff_availability_4 + 1
      end
      if result.staff_professionalism == 3
        @staff_availability_3 = @staff_availability_3 + 1
      end
      if result.staff_availability == 2
        @staff_availability_2 = @staff_availability_2 + 1
      end
      if result.staff_availability == 1
        @staff_availability_1 = @staff_availability_1 + 1
      end

      if result.staff_cleanliness == 4
        @staff_cleanliness_4 = @staff_cleanliness_4 + 1
      end
      if result.staff_cleanliness == 3
        @staff_cleanliness_3 = @staff_cleanliness_3 + 1
      end
      if result.staff_cleanliness == 2
        @staff_cleanliness_2 = @staff_cleanliness_2 + 1
      end
      if result.staff_cleanliness == 1
        @staff_cleanliness_1 = @staff_cleanliness_1 + 1
      end

      #---- 5 parte ------
      if result.met_by == 6
        @met_by_6 = @met_by_6 + 1
      end
      if result.met_by == 5
        @met_by_5 = @met_by_5 + 1
      end
      if result.met_by == 4
        @met_by_4 = @met_by_4 + 1
      end
      if result.met_by == 3
        @met_by_3 = @met_by_3 + 1
      end
      if result.met_by == 2
        @met_by_2 = @met_by_2 + 1
      end
      if result.met_by == 1
        @met_by_1 = @met_by_1 + 1
      end
    end    
  end

  def result_survey
    @survey = Survey.new
    @result1  = Survey.all
    @welcome_attention_4 = 0
    @welcome_attention_3 = 0
    @welcome_attention_2 = 0
    @welcome_attention_1 = 0

    @welcome_professionality_4 = 0
    @welcome_professionality_3 = 0
    @welcome_professionality_2 = 0
    @welcome_professionality_1 = 0

    @welcome_decoration_4 = 0
    @welcome_decoration_3 = 0
    @welcome_decoration_2 = 0
    @welcome_decoration_1 = 0

    @welcome_butler_4 = 0
    @welcome_butler_3 = 0
    @welcome_butler_2 = 0
    @welcome_butler_1 = 0

    @welcome_info_quality_4 = 0
    @welcome_info_quality_3 = 0
    @welcome_info_quality_2 = 0
    @welcome_info_quality_1 = 0

    @welcome_info_quality_4 = 0
    @welcome_info_quality_3 = 0
    @welcome_info_quality_2 = 0
    @welcome_info_quality_1 = 0

    #--- 2 parte encuesta-----

    @room_cleaning_4 = 0
    @room_cleaning_3 = 0
    @room_cleaning_2 = 0
    @room_cleaning_1 = 0

    @room_comfort_4 = 0
    @room_comfort_3 = 0
    @room_comfort_2 = 0
    @room_comfort_1 = 0

    @room_decoration_4 = 0
    @room_decoration_3 = 0
    @room_decoration_2 = 0
    @room_decoration_1 = 0

    @room_cleanliness_4 = 0
    @room_cleanliness_3 = 0
    @room_cleanliness_2 = 0
    @room_cleanliness_1 = 0

    @room_channels_4 = 0
    @room_channels_3 = 0
    @room_channels_2 = 0
    @room_channels_1 = 0

    @room_soundless_4 = 0
    @room_soundless_3 = 0
    @room_soundless_2 = 0
    @room_soundless_1 = 0

    #-- 3 parte ---- 

    @facilities_lobby_4 = 0
    @facilities_lobby_3 = 0
    @facilities_lobby_2 = 0
    @facilities_lobby_1 = 0

    #-- 4 parte ----

    @staff_professionalism_4 = 0
    @staff_professionalism_3 = 0
    @staff_professionalism_2 = 0
    @staff_professionalism_1 = 0

    @staff_simpathy_4 = 0
    @staff_simpathy_3 = 0
    @staff_simpathy_2 = 0
    @staff_simpathy_1 = 0

    @staff_speed_4 = 0
    @staff_speed_3 = 0
    @staff_speed_2 = 0
    @staff_speed_1 = 0

    @staff_effectiveness_4 = 0
    @staff_effectiveness_3 = 0
    @staff_effectiveness_2 = 0
    @staff_effectiveness_1 = 0

    @staff_availability_4 = 0
    @staff_availability_3 = 0
    @staff_availability_2 = 0
    @staff_availability_1 = 0

    @staff_cleanliness_4 = 0
    @staff_cleanliness_3 = 0
    @staff_cleanliness_2 = 0
    @staff_cleanliness_1 = 0

     #---- 5 parte ----- 
    @met_by_6 = 0
    @met_by_5 = 0
    @met_by_4 = 0
    @met_by_3 = 0
    @met_by_2 = 0
    @met_by_1 = 0


    @result1.each do |result|
      if result.welcome_attention == 4
        @welcome_attention_4 = @welcome_attention_4 + 1
      end
      if result.welcome_attention == 3
        @welcome_attention_3 = @welcome_attention_3 + 1
      end
      if result.welcome_attention == 2
        @welcome_attention_2 = @welcome_attention_2 + 1
      end
      if result.welcome_attention == 1
        @welcome_attention_1 = @welcome_attention_1 + 1
      end
      if result.welcome_professionality == 4
        @welcome_professionality_4 = @welcome_professionality_4 + 1
      end
      if result.welcome_professionality == 3
        @welcome_professionality_3 = @welcome_professionality_3 + 1
      end
      if result.welcome_professionality == 2
        @welcome_professionality_2 = @welcome_professionality_2 + 1
      end
      if result.welcome_professionality == 1
        @welcome_professionality_1 = @welcome_professionality_1 + 1
      end
      if result.welcome_decoration == 4
        @welcome_decoration_4 = @welcome_decoration_4 + 1
      end
      if result.welcome_decoration == 3
        @welcome_decoration_3 = @welcome_decoration_3 + 1
      end
      if result.welcome_decoration == 2
        @welcome_decoration_2 = @welcome_decoration_2 + 1
      end
      if result.welcome_decoration == 1
        @welcome_decoration_1 = @welcome_decoration_1 + 1
      end
      if result.welcome_butler == 4
        @welcome_butler_4 = @welcome_butler_4 + 1
      end
      if result.welcome_butler == 3
        @welcome_butler_3 = @welcome_butler_3 + 1
      end
      if result.welcome_butler == 2
        @welcome_butler_2 = @welcome_butler_2 + 1
      end
      if result.welcome_butler == 1
        @welcome_butler_1 = @welcome_butler_1 + 1
      end
      if result.welcome_info_quality == 4
        @welcome_info_quality_4 = @welcome_info_quality_4 + 1
      end
      if result.welcome_info_quality == 3
        @welcome_info_quality_3 = @welcome_info_quality_3 + 1
      end
      if result.welcome_info_quality == 2
        @welcome_info_quality_2 = @welcome_info_quality_2 + 1
      end
      if result.welcome_info_quality == 1
        @welcome_info_quality_1 = @welcome_info_quality_1 + 1
      end

      #-- 2 parte ------
      
      if result.room_cleaning == 4
        @room_cleaning_4 = @room_cleaning_4 + 1
      end
      if result.room_cleaning == 3
        @room_cleaning_3 = @room_cleaning_3 + 1
      end
      if result.room_cleaning == 2
        @room_cleaning_2 = @room_cleaning_2 + 1
      end
      if result.room_cleaning == 1
        @room_cleaning_1 = @room_cleaning_1 + 1
      end 

       if result.room_comfort == 4
        @room_comfort_4 = @room_comfort_4 + 1
      end
      if result.room_comfort == 3
        @room_comfort_3 = @room_comfort_3 + 1
      end
      if result.room_comfort == 2
        @room_comfort_2 = @room_comfort_2 + 1
      end
      if result.room_comfort == 1
        @room_comfort_1 = @room_comfort_1 + 1
      end

      if result.room_decoration == 4
        @room_decoration_4 = @room_decoration_4 + 1
      end
      if result.room_decoration == 3
        @room_decoration_3 = @room_decoration_3 + 1
      end
      if result.room_decoration == 2
        @room_decoration_2 = @room_decoration_2 + 1
      end
      if result.room_decoration == 1
        @room_decoration_1 = @room_decoration_1 + 1
      end 

      if result.room_cleanliness == 4
        @room_cleanliness_4 = @room_cleanliness_4 + 1
      end
      if result.room_cleanliness == 3
        @room_cleanliness_3 = @room_cleanliness_3 + 1
      end
      if result.room_cleanliness == 2
        @room_cleanliness_2 = @room_cleanliness_2 + 1
      end
      if result.room_cleanliness == 1
        @room_cleanliness_1 = @room_cleanliness_1 + 1
      end 

      if result.room_channels == 4
        @room_channels_4 = @room_channels_4 + 1
      end
      if result.room_channels == 3
        @room_channels_3 = @room_channels_3 + 1
      end
      if result.room_channels == 2
        @room_channels_2 = @room_channels_2 + 1
      end
      if result.room_channels == 1
        @room_channels_1 = @room_channels_1 + 1
      end

      if result.room_soundless == 4
        @room_soundless_4 = @room_soundless_4 + 1
      end
      if result.room_soundless == 3
        @room_soundless_3 = @room_soundless_3 + 1
      end
      if result.room_soundless == 2
        @room_soundless_2 = @room_soundless_2 + 1
      end
      if result.room_soundless == 1
        @room_soundless_1 = @room_soundless_1 + 1
      end

      #---- 3 parte-----

      if result.facilities_lobby == 4
        @facilities_lobby_4 = @facilities_lobby_4 + 1
      end
      if result.facilities_lobby == 3
        @facilities_lobby_3 = @facilities_lobby_3 + 1
      end
      if result.facilities_lobby == 2
        @facilities_lobby_2 = @facilities_lobby_2 + 1
      end
      if result.facilities_lobby == 1
        @facilities_lobby_1 = @facilities_lobby_1 + 1
      end

      #----- 4 parte ------  

      if result.staff_professionalism == 4
        @staff_professionalism_4 = @staff_professionalism_4 + 1
      end
      if result.staff_professionalism == 3
        @staff_professionalism_3 = @staff_professionalism_3 + 1
      end
      if result.staff_professionalism == 2
        @staff_professionalism_2 = @staff_professionalism_2 + 1
      end
      if result.staff_professionalism == 1
        @staff_professionalism_1 = @staff_professionalism_1 + 1
      end

      if result.staff_effectiveness == 4
        @staff_effectiveness_4 = @staff_effectiveness_4 + 1
      end
      if result.staff_effectiveness == 3
        @staff_effectiveness_3 = @staff_effectiveness_3 + 1
      end
      if result.staff_effectiveness == 2
        @staff_effectiveness_2 = @staff_effectiveness_2 + 1
      end
      if result.staff_effectiveness == 1
        @staff_effectiveness_1 = @staff_effectiveness_1 + 1
      end

      if result.staff_simpathy == 4
        @staff_simpathy_4 = @staff_simpathy_4 + 1
      end
      if result.staff_simpathy == 3
        @staff_simpathy_3 = @staff_simpathy_3 + 1
      end
      if result.staff_simpathy == 2
        @staff_simpathy_2 = @staff_simpathy_2 + 1
      end
      if result.staff_simpathy == 1
        @staff_simpathy_1 = @staff_simpathy_1 + 1
      end

      if result.staff_speed == 4
        @staff_speed_4 = @staff_speed_4 + 1
      end
      if result.staff_speed == 3
        @staff_speed_3 = @staff_speed_3 + 1
      end
      if result.staff_speed == 2
        @staff_speed_2 = @staff_speed_2 + 1
      end
      if result.staff_speed == 1
        @staff_speed_1 = @staff_speed_1 + 1
      end

      if result.staff_availability == 4
        @staff_availability_4 = @staff_availability_4 + 1
      end
      if result.staff_professionalism == 3
        @staff_availability_3 = @staff_availability_3 + 1
      end
      if result.staff_availability == 2
        @staff_availability_2 = @staff_availability_2 + 1
      end
      if result.staff_availability == 1
        @staff_availability_1 = @staff_availability_1 + 1
      end

      if result.staff_cleanliness == 4
        @staff_cleanliness_4 = @staff_cleanliness_4 + 1
      end
      if result.staff_cleanliness == 3
        @staff_cleanliness_3 = @staff_cleanliness_3 + 1
      end
      if result.staff_cleanliness == 2
        @staff_cleanliness_2 = @staff_cleanliness_2 + 1
      end
      if result.staff_cleanliness == 1
        @staff_cleanliness_1 = @staff_cleanliness_1 + 1
      end

      #---- 5 parte ------
      if result.met_by == 6
        @met_by_6 = @met_by_6 + 1
      end
      if result.met_by == 5
        @met_by_5 = @met_by_5 + 1
      end
      if result.met_by == 4
        @met_by_4 = @met_by_4 + 1
      end
      if result.met_by == 3
        @met_by_3 = @met_by_3 + 1
      end
      if result.met_by == 2
        @met_by_2 = @met_by_2 + 1
      end
      if result.met_by == 1
        @met_by_1 = @met_by_1 + 1
      end
    end    
  end

  # GET /surveys/new
  def new
    @survey = Survey.new
  end

  def validate
    @survey = Survey.new(survey_params)
    @already_answered = Survey.already_answered(survey_params)
    if @already_answered == 0
      @survey.errors.add(:base, 'Email es incorrecto')
    elsif @already_answered == 1
      @survey.errors.add(:base, 'N° de habitacion tiene que ser un numero')
    end
  end

  # GET /surveys/1/edit
  def edit
  end

  # POST /surveys
  def create
    @survey = Survey.new(survey_params)
    @survey.ip = request.remote_ip
    @survey.total_excellent = 0
    @survey.total_good = 0
    @survey.total_regular = 0
    @survey.total_bad = 0

    if @survey.save
      SurveyMailer.survey_completed(@survey).deliver_now
      redirect_to root_path(encuesta: 'enviada')
    else
      errors = []
      @survey.errors.messages.values.each { |e| errors.push(e.first) }
      errors.include?('no puede estar en blanco')
      @survey.errors.add(:base, 'Favor llenar campos requeridos.')
      render :new
    end
  end

  # PATCH/PUT /surveys/1
  def update
    if @survey.update(survey_params)
      redirect_to @survey, notice: 'Survey was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /surveys/1
  def destroy
    @survey.destroy
    redirect_to surveys_url, notice: 'Survey was successfully destroyed.'
  end

  def destroy_multiple
    Survey.destroy redefine_ids(params[:multiple_ids])
    redirect_to surveys_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_survey
      @survey = Survey.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def survey_params
      params.require(:survey).permit(:ip, :name, :email, :room, :checkin, :checkout, :comment, :welcome_attention, :welcome_professionality, :welcome_decoration, :welcome_butler, :welcome_info_quality, :room_cleaning, :room_comfort, :room_decoration, :room_cleanliness, :room_channels, :room_soundless, :facilities_lobby, :facilities_business_center, :facilities_gym, :facilities_stores, :facilities_other, :facilities_other_value, :lobby_breakfast, :lobby_menu, :lobby_waiting, :lobby_service, :lobby_staff, :lobby_quality_price, :terrace_breakfast, :terrace_menu, :terrace_waiting, :terrace_service, :terrace_staff, :terrace_quality_price, :staff_professionalism, :staff_simpathy, :staff_speed, :staff_effectiveness, :staff_availability, :staff_cleanliness, :met_by)
    end

    def resolve_layout
      case action_name
      when 'show', 'index', 'result_survey', 'search_surveys_range_days'
        'admin/application'
      else
        'frontend/application'
      end
    end
end
