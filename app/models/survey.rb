# Generado por keppler
require 'elasticsearch/model'
class Survey < ActiveRecord::Base
  include SurveysHelper
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  before_create :set_totals , on: :create

  validates_presence_of :welcome_attention, :welcome_professionality,
                        :welcome_decoration, :welcome_butler,
                        :welcome_info_quality, :room_cleaning,
                        :room_comfort, :room_decoration,
                        :room_cleanliness, :room_channels,
                        :room_soundless, :facilities_lobby,
                        :staff_professionalism, :staff_simpathy,
                        :staff_speed, :staff_effectiveness,
                        :staff_availability, :staff_cleanliness,
                        :met_by, :room, :name, :email
  validate :valid_date_range_required

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def set_totals
    Survey.countable_attributes.each do |attr|
      case self[attr]
      when 1
        self.total_bad += 1
      when 2
        self.total_regular += 1
      when 3
        self.total_good += 1
      when 4
        self.total_excellent += 1
      end
    end
  end

  def self.searching(query)
    if query
      search(query query).records.order(id: :desc)
    else
      order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match: { query: query, fields: [:name, :email, :checkin, :checkout, :comment], operator: :and } }, sort: { id: "desc" }, size: count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: id.to_s,
      ip:  ip.to_s,
      name:  name.to_s,
      email:  email.to_s,
      room:  room.to_s,
      checkin:  checkin.to_s,
      checkout:  checkout.to_s,
      comment:  comment.to_s,
      met_by:  met_by.to_s,
    }.as_json
  end

  def self.already_answered(params)
    #where('created_at > ?', (DateTime.now - 1.day)).select do |survey|
    #  survey.room == params[:room] && survey.email == params[:email]
    #end

    if params[:email] =~ /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
      if params[:room] =~ /\A[-+]?[0-9]+(\.[0-9]+)?\z/
        return 2
      else
        return 1
      end
    else
      return 0
    end 
  end

  def valid_date_range_required
    unless checkin.blank? || checkout.blank?
      errors.add(:base, "El check-out no puede ser despues del check-in") if checkin > checkout
    end
  end

  def valid_check_in
    unless checkin.blank? || checkout.blank?
      errors.add(:base, "El check-in que usted ingresó no es valido") if checkin < Date.parse(Time.new.to_s)
      errors.add(:base, "El check-in que usted ingresó no es valido") if checkout < Date.parse(Time.new.to_s)
    end
  end

  def status
    {
      'very_dissatisfied' => total_bad,
      'dissatisfied' => total_regular,
      'satisfied' => total_good,
      'very_satisfied' => total_excellent
    }
  end

  def status_icon
    status.max.first
  end

  def status_color
    case status.max.first
    when 'very_dissatisfied'
      'red-text'
    when 'dissatisfied'
      'deep-orange-text'
    when 'satisfied'
      'blue-text'
    when 'very_satisfied'
      'green-text'
    end
  end

  def checks_label
    if checkin.blank? && checkout
      'Check-out'
    elsif checkout.blank? && checkin
      'Check-in'
    else
      'Check-in / Check-out'
    end
  end

  def checks
    if checkin && checkout
      "#{checkin} / #{checkout}"
    elsif checkin.blank? && checkout
      checkout
    elsif checkout.blank? && checkin
      checkin
    end
  end

  def self.countable_attributes
    %w(
      welcome_attention welcome_professionality welcome_decoration
      welcome_butler welcome_info_quality room_cleaning room_comfort
      room_decoration room_cleanliness room_channels room_soundless
      facilities_lobby facilities_business_center
      facilities_gym facilities_stores facilities_other
      lobby_breakfast lobby_menu lobby_waiting
      lobby_service lobby_staff lobby_quality_price
      terrace_breakfast terrace_menu terrace_waiting
      terrace_service terrace_staff terrace_quality_price
      staff_professionalism staff_simpathy staff_speed
      staff_effectiveness staff_availability staff_cleanliness
    )
  end
end
#Survey.import
