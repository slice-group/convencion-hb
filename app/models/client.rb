#Generado por keppler
require 'elasticsearch/model'
class Client < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  validates_presence_of :name, :email, :telephone, :documentation
  has_many :reservations, dependent: :destroy
  has_many :events, dependent: :destroy

  #validaciones para telefono
  #validates :telephone, :length => { :minimum => 11, :maximum => 13 }
  validates_numericality_of :telephone, :only_integer => true

  #validacion de email
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }


  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:documentation, :name, :company, :email, :telephone] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      documentation:  self.documentation.to_s,
      name:  self.name,
      company:  self.company.to_s,
      email:  self.email.to_s,
      telephone:  self.telephone.to_s,
    }.as_json
  end

end
#Client.import
