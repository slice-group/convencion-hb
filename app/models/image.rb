class Image < ActiveRecord::Base
    mount_uploader :img, ImgUploader
    #after_update :remove_image
    belongs_to :room

    def remove_image
      if !self.img.nil?
        image = File.dirname(Rails.root.join(self.img.to_s))
        FileUtils.rm_rf(image)
      end
    end

end
