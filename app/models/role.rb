class Role < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true
  validates_uniqueness_of :name

  scopify

  def change_name
  	case self.name
  	when 'admin'
  		return 'Administrador'
  	when 'reservation'
  		return 'Reservación'
  	when 'commercialization'
  		return 'Comercialización'
  	end
  end
end
