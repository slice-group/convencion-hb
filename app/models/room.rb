#Generado por keppler
require 'elasticsearch/model'
class Room < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  mount_uploader :cover, ImgUploader
  before_save :create_permalink
  has_many :images, :dependent => :destroy
  has_many :reservations, through: :orders
  has_many :orders
  has_many :events_rooms
  accepts_nested_attributes_for :images, :reject_if => :all_blank, :allow_destroy => true
  validates_presence_of :name, :price, :description, :cover, on: :update
  validates_uniqueness_of :name


  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:id, :name, :price, :description] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name,
      price:  self.price.to_s,
      description:  self.description,
    }.as_json
  end

  def create_permalink
    self.permalink=self.name.downcase.parameterize
  end

end

#Room.import
