#Generado por keppler
require 'elasticsearch/model'
class Banner < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  mount_uploader :image, ImgUploader

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name.to_s,
      image:  self.image.to_s,
      external_link:  self.external_link.to_s,
      public:  self.public.to_s,
    }.as_json
  end

end
#Banner.import
