function removeHash () { 
	history.pushState("", document.title, window.location.pathname + window.location.search);
}

function scrollToAnchor(anchor){
	$('#'+anchor).animatescroll({
		scrollSpeed:1500
	})
	removeHash ();
}

$( document ).ready(function() {

	var sections = $('section'), nav = $('.navbar-nav'), nav_height = nav.outerHeight();
 
	$(window).on('scroll', function () {
		var cur_pos = $(this).scrollTop();
	 
		sections.each(function() {
			var top = $(this).offset().top - 80,
				bottom = top + $(this).outerHeight();
	 
			if (cur_pos >= top && cur_pos <= bottom) {
				nav.find('li').removeClass('current');
				sections.removeClass('current');
	 
				$(this).addClass('current');
				nav.find('#link-'+$(this).attr('id')).addClass('current');
			}
		});
	});
	
});
function Map(){
	if($('#front-navbar').css('top') == '-300px'){
		$('#front-navbar').animate( {top: 0});
	}
	else{
		$('#front-navbar').animate( {top: '-300px'});
	}
}

$(document).click(function(){
	$('.navbar-collapse').collapse('hide');
	if($('#front-navbar').css('top') == '0px'){
		$('#front-navbar').animate( {top: '-300px'});
	}

});
