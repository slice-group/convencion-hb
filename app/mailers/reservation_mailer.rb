class ReservationMailer < ApplicationMailer
  default from: "reservas@convencionhb.com"

	def reservation_admin(reservation)
		@client_name = reservation.client.name
		@link = reservation_path(reservation.id)
    @terms = Term.find_by_name(:reservaciones)
		mail(to: ['recepcion@covencionhb.com', 'reservas@convencionhb.com'], subject: 'Nueva solicitud de Reservación.')
	end

	def reservation_user(reservacion)
		@reservacion = reservacion
    @terms = Term.find_by_name(:reservaciones)
		mail(to: @reservacion.client.email, subject: 'Has enviado una solicitud a Convención Hotel Boutique.')
	end

end
