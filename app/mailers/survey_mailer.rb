class SurveyMailer < ApplicationMailer
	default from: "reservas@convencionhb.com"
	add_template_helper(SurveysHelper)

  def survey_completed(survey)
  	@survey = survey
  	attachments["Encuesta_#{@survey.created_at.strftime('%d-%m-%y_%I:%M')}.pdf"] = WickedPdf.new.pdf_from_string(
      render_to_string(pdf: 'Encuesta', template: 'surveys/show.pdf.haml'), {  }
    )
    mail to: "operaciones@convencionhb.com", subject: 'Nueva encuesta.'
  end
end
