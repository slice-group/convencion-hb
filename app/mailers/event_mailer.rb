class EventMailer < ApplicationMailer
  default from: "eventos@convencionhb.com"

  def event_admin(evento)
    @evento = evento
    @terms = Term.find_by_name(:eventos)
    mail(
      to: 'eventos@convencionhb.com',
      subject: "#{evento.client.name.humanize} ha enviado una solicitud de Evento."
      )
  end

  def event_user(evento)
    @evento = evento
    @terms = Term.find_by_name(:eventos)
    mail(
      to: @evento.client.email,
      subject: "Has enviado una solicitud a Convención Hotel Boutique."
      )
  end
end
