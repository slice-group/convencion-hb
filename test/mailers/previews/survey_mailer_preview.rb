# Preview all emails at http://localhost:3000/rails/mailers/survey_mailer
class SurveyMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/survey_mailer/survey_completed
  def survey_completed
    SurveyMailer.survey_completed
  end

end
