require 'test_helper'

class SurveyMailerTest < ActionMailer::TestCase
  test "survey_completed" do
    mail = SurveyMailer.survey_completed
    assert_equal "Survey completed", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
