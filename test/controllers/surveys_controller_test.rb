require 'test_helper'

class SurveysControllerTest < ActionController::TestCase
  setup do
    @survey = surveys(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:surveys)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create survey" do
    assert_difference('Survey.count') do
      post :create, survey: { checkin: @survey.checkin, checkout: @survey.checkout, comment: @survey.comment, email: @survey.email, facilities_business_center: @survey.facilities_business_center, facilities_gym: @survey.facilities_gym, facilities_lobby: @survey.facilities_lobby, facilities_other: @survey.facilities_other, facilities_other_value: @survey.facilities_other_value, facilities_stores: @survey.facilities_stores, ip: @survey.ip, lobby_breakfast: @survey.lobby_breakfast, lobby_manu: @survey.lobby_manu, lobby_quality_price: @survey.lobby_quality_price, lobby_service: @survey.lobby_service, lobby_staff: @survey.lobby_staff, lobby_waiting: @survey.lobby_waiting, met_by: @survey.met_by, name: @survey.name, room: @survey.room, room_channels: @survey.room_channels, room_cleaning: @survey.room_cleaning, room_cleanliness: @survey.room_cleanliness, room_comfort: @survey.room_comfort, room_decoration: @survey.room_decoration, room_soundless: @survey.room_soundless, staff_availability: @survey.staff_availability, staff_cleanliness: @survey.staff_cleanliness, staff_effectiveness: @survey.staff_effectiveness, staff_professionalism: @survey.staff_professionalism, staff_simpathy: @survey.staff_simpathy, staff_speed: @survey.staff_speed, terrace_breakfast: @survey.terrace_breakfast, terrace_manu: @survey.terrace_manu, terrace_quality_price: @survey.terrace_quality_price, terrace_service: @survey.terrace_service, terrace_staff: @survey.terrace_staff, terrace_waiting: @survey.terrace_waiting, welcome_attention: @survey.welcome_attention, welcome_butler: @survey.welcome_butler, welcome_decoration: @survey.welcome_decoration, welcome_info_quality: @survey.welcome_info_quality, welcome_professionality: @survey.welcome_professionality }
    end

    assert_redirected_to survey_path(assigns(:survey))
  end

  test "should show survey" do
    get :show, id: @survey
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @survey
    assert_response :success
  end

  test "should update survey" do
    patch :update, id: @survey, survey: { checkin: @survey.checkin, checkout: @survey.checkout, comment: @survey.comment, email: @survey.email, facilities_business_center: @survey.facilities_business_center, facilities_gym: @survey.facilities_gym, facilities_lobby: @survey.facilities_lobby, facilities_other: @survey.facilities_other, facilities_other_value: @survey.facilities_other_value, facilities_stores: @survey.facilities_stores, ip: @survey.ip, lobby_breakfast: @survey.lobby_breakfast, lobby_manu: @survey.lobby_manu, lobby_quality_price: @survey.lobby_quality_price, lobby_service: @survey.lobby_service, lobby_staff: @survey.lobby_staff, lobby_waiting: @survey.lobby_waiting, met_by: @survey.met_by, name: @survey.name, room: @survey.room, room_channels: @survey.room_channels, room_cleaning: @survey.room_cleaning, room_cleanliness: @survey.room_cleanliness, room_comfort: @survey.room_comfort, room_decoration: @survey.room_decoration, room_soundless: @survey.room_soundless, staff_availability: @survey.staff_availability, staff_cleanliness: @survey.staff_cleanliness, staff_effectiveness: @survey.staff_effectiveness, staff_professionalism: @survey.staff_professionalism, staff_simpathy: @survey.staff_simpathy, staff_speed: @survey.staff_speed, terrace_breakfast: @survey.terrace_breakfast, terrace_manu: @survey.terrace_manu, terrace_quality_price: @survey.terrace_quality_price, terrace_service: @survey.terrace_service, terrace_staff: @survey.terrace_staff, terrace_waiting: @survey.terrace_waiting, welcome_attention: @survey.welcome_attention, welcome_butler: @survey.welcome_butler, welcome_decoration: @survey.welcome_decoration, welcome_info_quality: @survey.welcome_info_quality, welcome_professionality: @survey.welcome_professionality }
    assert_redirected_to survey_path(assigns(:survey))
  end

  test "should destroy survey" do
    assert_difference('Survey.count', -1) do
      delete :destroy, id: @survey
    end

    assert_redirected_to surveys_path
  end
end
