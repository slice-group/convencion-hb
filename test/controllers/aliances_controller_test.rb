require 'test_helper'

class AliancesControllerTest < ActionController::TestCase
  setup do
    @aliance = aliances(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:aliances)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create aliance" do
    assert_difference('Aliance.count') do
      post :create, aliance: { external_link: @aliance.external_link, image: @aliance.image, name: @aliance.name }
    end

    assert_redirected_to aliance_path(assigns(:aliance))
  end

  test "should show aliance" do
    get :show, id: @aliance
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @aliance
    assert_response :success
  end

  test "should update aliance" do
    patch :update, id: @aliance, aliance: { external_link: @aliance.external_link, image: @aliance.image, name: @aliance.name }
    assert_redirected_to aliance_path(assigns(:aliance))
  end

  test "should destroy aliance" do
    assert_difference('Aliance.count', -1) do
      delete :destroy, id: @aliance
    end

    assert_redirected_to aliances_path
  end
end
