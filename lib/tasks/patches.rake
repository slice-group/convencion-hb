namespace :patches do
	desc "TODO"

	task add_terms: :environment do
    [:reservaciones, :eventos].each do |term|
    	Term.create name: term
    	puts "#{term} creado"
    end
	end

	task reset_terms: :environment do
		Term.all.each do |term|
			term.update(description: "")
		end
	end

	task delete_two_rooms: :environment do
    Room.find_by_name("Suite Junior").destroy
	  Room.find_by_name("Suite Presidencial").destroy
		puts "Suite Presidencial y Suite Junior han sido eliminadas"
	end
end
